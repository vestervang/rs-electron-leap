import 'zone.js/dist/zone-mix';
import 'reflect-metadata';
import '../polyfills';
import {BrowserModule} from '@angular/platform-browser';
import {Injectable, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {HttpClientModule, HttpClient, HttpHeaders} from '@angular/common/http';


// NG Translate
import {TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {ElectronService} from './providers/electron.service';

// import {WebviewDirective} from './directives/webview.directive';

import {AppComponent} from './app.component';
import {CoreModule} from './core/core.module';
import {Apollo, ApolloModule} from 'apollo-angular';
import {HttpLinkModule, HttpLink} from 'apollo-angular-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {setContext} from 'apollo-link-context';
import {ApolloLink} from 'apollo-link';
import {onError} from 'apollo-link-error';
import {HttpBatchLinkModule, HttpBatchLink} from 'apollo-angular-link-http-batch';

import {AuthService} from './core/services/auth.service';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/translations/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        // WebviewDirective
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ApolloModule,
        HttpLinkModule,
        HttpBatchLinkModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (HttpLoaderFactory),
                deps: [HttpClient]
            }
        }),
        CoreModule,
    ],
    providers: [
        ElectronService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

    constructor(protected apollo: Apollo, protected httpLink: HttpLink, protected authService: AuthService) {

        // Non batching http link
        const http = httpLink.create({
            uri: 'http://api.ql.local/graphql',
            method: 'POST',
        });

        // const authLink = setContext((_, {headers}) => {
        //
        //     console.log(_);
        //     console.log(headers);
        //     const token = 'fjklsdjfklds';
        //
        //     // console.log(authService.token);
        //
        //     return {
        //         headers: {
        //             ...headers,
        //             authorization: token ? `Bearer ${token}` : null,
        //         }
        //     };
        // });
        //
        // const refreshToken = onError(({response, operation}) => {
        //     // console.log('Error response:');
        //     // console.log(response);
        //     //
        //     // console.log('Error response:');
        //     // console.log(operation);
        // });
        // authLink, refreshToken,
        // refreshToken, authLink,

        const authMiddleware = new ApolloLink((operation, forward) => {
            if (this.authService.token) {
                operation.setContext({
                    headers: new HttpHeaders().set('Authorization', `Bearer ${this.authService.token}`),
                });
            }
            return forward(operation);
        });

        const link = ApolloLink.from([
            authMiddleware, http
        ]);

        apollo.create({
            link: link,
            cache: new InMemoryCache(),
            defaultOptions: {
                watchQuery: {
                    errorPolicy: 'all'
                },
                mutate: {
                    errorPolicy: 'all'
                }
            }
        });

    }
}
