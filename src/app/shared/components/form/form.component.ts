import {Component, OnInit, AfterViewInit, Input, ElementRef, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Form} from '../../classes/form';
import {InputField} from '../../classes/input_fields/input-field';

import {faCheck, faTimes} from '@fortawesome/pro-light-svg-icons';
import {Router} from '@angular/router';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.sass']
})

export class FormComponent implements OnInit, AfterViewInit {

    @ViewChild('submit') submitButton: ElementRef;

    private _form = new FormGroup({});
    private _parentComponent: any;
    private _classList: Array<string>;
    private _inputFields: Array<InputField>;
    private _serverErrors: Array<string>;
    private _redirectUrl: string;

    private _successIcon = faCheck;
    private _errorIcon = faTimes;
    private _submitFunction: any;
    private _submitButtonText: any;
    private _currentSubmitIcon: any;
    private _submitButtonWidth: number;
    private _submitButtonHeight: number;
    private _onCooldown = false;
    private _submitButtonResetTime = 2000;
    private _resetSubmitButtonInterval: any;
    state: FormStates;


    constructor(
        protected router: Router
    ) {
    }

    ngOnInit() {
        this.state = FormStates.Ready;
        this._serverErrors = [];
    }

    ngAfterViewInit() {
        this._submitButtonWidth = this.submitButton.nativeElement.clientWidth;
        this._submitButtonHeight = this.submitButton.nativeElement.clientHeight;
        this.submitButton.nativeElement.style.width = this._submitButtonWidth + 'px';
        this.submitButton.nativeElement.style.height = this._submitButtonHeight + 'px';
    }

    async submitFunction(event) {
        this.submitButton.nativeElement.style.width = this._submitButtonHeight + 'px';
        this.submitButton.nativeElement.style.height = this._submitButtonHeight + 'px';

        this._onCooldown = true;

        this.state = FormStates.Submitting;
        let response: any;
        this._serverErrors = [];

        response = await this._parentComponent[this._submitFunction](this._form.value);

        if (!response) {
            return;
        }

        // Start reset interval
        this._resetSubmitButtonInterval = setInterval(() => {
            this.resetSubmitButton();
        }, this._submitButtonResetTime);

        if (typeof response.errors !== 'undefined' && response.errors.length > 0) {
            this.makeSubmitButtonError();

            for (let i = 0; i < response.errors.length; i++) {
                this._serverErrors.push(response.errors[i].message);
            }
            return;
        }

        this.makeSubmitButtonSuccess();

        if (this._redirectUrl) {
            this.router.navigate([this._redirectUrl]);
        }
    }

    makeSubmitButtonSuccess() {
        // this.resetSubmitButtonDimensions();
        this._currentSubmitIcon = this._successIcon;
        this.state = FormStates.Success;
    }

    makeSubmitButtonError() {
        // this.resetSubmitButtonDimensions();
        this.state = FormStates.Error;
        this._currentSubmitIcon = this._errorIcon;
    }

    resetSubmitButtonDimensions() {
        this.submitButton.nativeElement.style.width = this._submitButtonWidth + 'px';
        this.submitButton.nativeElement.style.height = this._submitButtonHeight + 'px';
    }

    resetSubmitButton() {
        clearInterval(this._resetSubmitButtonInterval);
        this.resetSubmitButtonDimensions();
        this.state = FormStates.Ready;
        this._currentSubmitIcon = null;
        this._onCooldown = false;
    }

    @Input()
    set formData(data: Form) {
        this._parentComponent = data.component;
        this._classList = data.classes;
        this._submitFunction = data.submitFunction;
        this._inputFields = data.inputFields;
        this._submitButtonText = data.submitButtonText;
        this._redirectUrl = data.redirectUrl;
    }

    get form() {
        return this._form;
    }

    get classList() {
        return this._classList;
    }

    get inputFields() {
        return this._inputFields;
    }

    get submitButtonText() {
        return this._submitButtonText;
    }

    get serverErrors() {
        return this._serverErrors;
    }

    get submitButtonIcon() {
        return this._currentSubmitIcon;
    }

    get onCooldown() {
        return this._onCooldown;
    }
}

enum FormStates {
    Ready = 1,
    Submitting = 2,
    Success = 3,
    Error = 4
}
