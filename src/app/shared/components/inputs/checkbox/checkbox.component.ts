import {Component, Input, OnInit} from '@angular/core';
import {InputField} from '../../../classes/input_fields/input-field';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-checkbox',
    templateUrl: './checkbox.component.html',
    styleUrls: ['./checkbox.component.sass']
})
export class CheckboxComponent implements OnInit {

    protected _form: FormGroup;
    protected _control: FormControl;
    protected _checked: boolean;

    constructor() {
    }

    ngOnInit() {
    }

    @Input()
    set form(formGroup: FormGroup) {
        this._form = formGroup;
    }

    @Input()
    set inputData(inputData) {
        this._control = new FormControl(inputData.value, inputData.validators);
        this._form.addControl(inputData.name, this._control);
    }

}
