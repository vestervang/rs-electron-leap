import {Component, OnInit, OnDestroy, Input, ChangeDetectorRef} from '@angular/core';
import {FormGroup, FormControl, ValidatorFn, AsyncValidatorFn, Validators} from '@angular/forms';
import {InputField} from '../../classes/input_fields/input-field';
import {InputType} from 'zlib';
import {Checkbox} from '../../classes/input_fields/checkbox';

@Component({
    selector: 'app-inputs',
    templateUrl: './inputs.component.html',
    styleUrls: ['./inputs.component.scss']
})
export class InputsComponent implements OnInit {

    inputTypes = InputTypes;
    protected _form: FormGroup;
    protected _inputData: InputField | Checkbox;
    protected _type: InputTypes;

    constructor() {
    }

    ngOnInit() {
    }

    protected getInputGroup(className) {
        switch (className) {
            case 'inputfield':
                return this.inputTypes.Input;
            case 'checkbox':
                return this.inputTypes.Checkbox;
        }
    }

    /*************************
     *************************
     *
     * Setters
     *
     *************************
     *************************/

    @Input()
    set form(formGroup: FormGroup) {
        this._form = formGroup;
    }

    @Input()
    set inputData(inputData: InputField | Checkbox) {
        const className = inputData.constructor.name.toLowerCase();

        this._type = this.getInputGroup(className);
        this._inputData = inputData;
    }

    /*************************
     *************************
     *
     * Getters
     *
     *************************
     *************************/

    get form() {
        return this._form;
    }

    get inputData() {
        return this._inputData;
    }

    get type() {
        return this._type;
    }
}

enum InputTypes {
    Input = 1,
    Checkbox = 2,
}

