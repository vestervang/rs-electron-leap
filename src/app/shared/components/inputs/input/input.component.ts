import {Component, Input, OnInit} from '@angular/core';
import {InputField} from '../../../classes/input_fields/input-field';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-input',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.sass']
})
export class InputComponent implements OnInit {

    protected _form: FormGroup;
    protected _control: FormControl;
    protected _inputData: InputField;

    protected _hasContent = true;

    constructor() {
    }

    ngOnInit() {
    }

    blur(event) {
        if (event.target.value === '') {
            this._hasContent = false;
        } else {
            this._hasContent = true;
        }
    }

    @Input()
    set form(formGroup: FormGroup) {
        this._form = formGroup;
    }

    @Input()
    set inputData(inputData: InputField) {
        this._inputData = inputData;

        if (inputData.value === null || inputData.value === '') {
            this._hasContent = false;
        }

        this._control = new FormControl(inputData.value, inputData.validators);
        this._form.addControl(inputData.name, this._control);
    }

    get form() {
        return this._form;
    }

    get control() {
        return this._control;
    }

    get hasContent() {
        return this._hasContent;
    }

    get name() {
        return this._inputData.name;
    }

    get type() {
        return this._inputData.type;
    }

    get placeholder() {
        return this._inputData.placeholder;
    }

    get value() {
        return this._inputData.value;
    }

}
