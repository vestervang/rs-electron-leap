import {FormGroup, Validators} from '@angular/forms';

export class InputField {
    protected _name: string;
    protected _type: string;
    protected _value: any;
    protected _placeholder: string;
    protected _validators: Validators;

    constructor(name, type, value = null, placeholder = null, validators = null) {
        this._name = name;
        this._type = type;
        this._value = value;
        this._placeholder = placeholder;
        this._validators = validators;
    }

    get name() {
        return this._name;
    }

    get type() {
        return this._type;
    }

    get value() {
        return this._value;
    }

    get placeholder() {
        return this._placeholder;
    }

    get validators() {
        return this._validators;
    }
}
