import {Validators} from '@angular/forms';

export class Checkbox {
    protected _name: string;
    protected _label: string;
    protected _validators: Validators;

    constructor(name, label = null, validators = null) {
        this._name = name;
        this._label = label;
        this._validators = validators;
    }

    get name() {
        return this._name;
    }

    get label() {
        return this._label;
    }

    get validators() {
        return this._validators;
    }
}
