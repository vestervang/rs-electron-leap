import {User as IUser} from '../interfaces/user';

export class User implements IUser {
    protected _id: number;
    protected _email: string;
    protected _firstName: string;
    protected _lastName: string;
    protected _username: string;

    constructor(id: number, firstName: string, lastName: string, email: string, username: string) {
        this._id = id;
        this._firstName = firstName;
        this._lastName = lastName;
        this._email = email;
        this._username = username;
    }

    get id() {
        return this._id;
    }

    get email() {
        return this._email;
    }

    get firstName() {
        return this._firstName;
    }

    get lastName() {
        return this._lastName;
    }

    get username() {
        return this._username;
    }
}
