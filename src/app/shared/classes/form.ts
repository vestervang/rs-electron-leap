import get = Reflect.get;
import {InputField} from './input_fields/input-field';
import {Checkbox} from './input_fields/checkbox';

export class Form {
    protected readonly _classes: Array<string>;
    protected readonly _component: any;
    protected readonly _submitFunction: string;
    protected readonly _inputFields: Array<InputField | Checkbox>;
    protected readonly _submitButtonText: string;
    protected readonly _redirectUrl: string;

    constructor(
        classes: Array<string>,
        component: any,
        submitFunction: string,
        inputFields: Array<InputField | Checkbox>,
        submitButtonText: string,
        redirectUrl?: string
    ) {
        this._classes = classes;
        this._component = component;
        this._submitFunction = submitFunction;
        this._inputFields = inputFields;
        this._submitButtonText = submitButtonText;
        this._redirectUrl = redirectUrl;
    }

    get classes() {
        return this._classes;
    }

    get component() {
        return this._component;
    }

    get submitFunction() {
        return this._submitFunction;
    }

    get inputFields() {
        return this._inputFields;
    }

    get submitButtonText() {
        return this._submitButtonText;
    }

    get redirectUrl() {
        return this._redirectUrl;
    }
}