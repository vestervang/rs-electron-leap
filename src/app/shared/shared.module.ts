import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {HttpLinkModule} from 'apollo-angular-link-http';
import {ApolloModule} from 'apollo-angular';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ReactiveFormsModule} from '@angular/forms';
import {InputsComponent} from './components/inputs/inputs.component';
import {FormComponent} from './components/form/form.component';
import { CheckboxComponent } from './components/inputs/checkbox/checkbox.component';
import { InputComponent } from './components/inputs/input/input.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ApolloModule,
        HttpLinkModule,
        FontAwesomeModule,
    ],
    exports: [
        TranslateModule,
        FontAwesomeModule,
        InputsComponent,
        FormComponent,
        CheckboxComponent,
    ],
    declarations: [
        InputsComponent,
        FormComponent,
        CheckboxComponent,
        InputComponent,
    ]
})
export class SharedModule {
}
