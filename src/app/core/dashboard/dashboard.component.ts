import {Component, OnInit} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Validators} from '@angular/forms';
import {Form} from '../../shared/classes/form';
import {InputField} from '../../shared/classes/input_fields/input-field';
import {Checkbox} from '../../shared/classes/input_fields/checkbox';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

    form = new Form(
        ['login-form'],
        this,
        'login',
        [
            new InputField('identifier', 'text', '', 'Identifier'),
            new InputField('password', 'password', '', 'Password'),
            new Checkbox('checkbox', 'Checkbox'),
        ],
        'login');

    constructor(public auth: AuthService) {
    }

    ngOnInit() {
        console.log(this.auth.user);
    }

}
