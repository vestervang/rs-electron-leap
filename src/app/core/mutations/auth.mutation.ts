import gql from 'graphql-tag';

export const LoginMutation = gql`
    mutation($identifier: String! $password: String!) {
        login(identifier:$identifier password: $password){
            auth_token{
                token_type
                expires_in
                access_token
            }
            employee{
                id
                username
                email
                first_name
                last_name
            }
        }
    }
`;
