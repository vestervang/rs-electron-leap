import {Injectable} from '@angular/core';

import {Apollo} from 'apollo-angular';
import {BehaviorSubject, Observable} from 'rxjs';
import {LoginMutation} from '../mutations/auth.mutation';
import {Router} from '@angular/router';
import {User} from '../../shared/classes/user';
import {User as IUser} from '../../shared/interfaces/user';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    protected USER_KEY = 'user';
    protected TOKEN_KEY = 'token';

    protected _user: User;
    protected _token: string;
    protected _isAuthenticated = new BehaviorSubject(false);

    constructor(
        protected apollo: Apollo,
        protected router: Router
    ) {
        this._token = this.getTokenFromStorage();
        this._user = this.getUserFromStorage();
        this.autoLogin();
    }

    login(idintifier: string, password: string) {
        return this.apollo.mutate({
            mutation: LoginMutation,
            variables: {
                identifier: idintifier,
                password: password
            }
        });
    }

    autoLogin() {
        if (!this._user && !this._token) {
            return;
        }

        this._isAuthenticated.next(true);
        this.router.navigate(['/dashboard']);
    }

    saveUserData(loginData: any) {
        this.saveToken(loginData.auth_token.access_token);
        this.saveUser(loginData.employee);

        this._isAuthenticated.next(true);
    }

    logout() {
        this.deleteAuthData();
        this._isAuthenticated.next(false);
        this.router.navigate(['/']);
    }

    get token() {
        return this._token;
    }

    get isAuthenticated(): Observable<boolean> {
        return this._isAuthenticated.asObservable();
    }

    get user(): User {
        return this._user;
    }

    protected deleteAuthData() {
        localStorage.removeItem(this.TOKEN_KEY);
        localStorage.removeItem(this.USER_KEY);
        this._user = null;
        this._token = null;
    }

    protected saveToken(token) {
        localStorage.setItem(this.TOKEN_KEY, JSON.stringify(token));
        this._token = token;
    }

    protected getTokenFromStorage() {
        return JSON.parse(localStorage.getItem(this.TOKEN_KEY));
    }

    protected saveUser(user) {
        localStorage.setItem(this.USER_KEY, JSON.stringify(user));
        this._user = this.getUserFromStorage();
    }

    protected getUserFromStorage(): User {
        const userFromStorage = <IUser>JSON.parse(localStorage.getItem(this.USER_KEY));

        if (!userFromStorage) {
            return null;
        }
        return new User(
            userFromStorage.id,
            userFromStorage.first_name,
            userFromStorage.last_name,
            userFromStorage.email,
            userFromStorage.username,
        );

    }
}
