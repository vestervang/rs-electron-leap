import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {EmployeeList} from '../queries/employee.query';

@Injectable({
    providedIn: 'root'
})
export class EmployeeService {

    constructor(
        protected apollo: Apollo
    ) {
    }

    getAllEmployees() {

    }
}
