import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators, NgForm, ValidatorFn, FormControl} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

import {AuthService} from '../services/auth.service';
import {HeaderService} from '../services/header.service';
import {Form} from '../../shared/classes/form';
import {InputField} from '../../shared/classes/input_fields/input-field';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    form = new Form(
        ['login-form'],
        this,
        'login',
        [
            new InputField('identifier', 'text', 'admin@admin.com', 'Identifier', Validators.compose([Validators.required, Validators.minLength(2)])),
            new InputField('password', 'password', 'secret', 'Password', Validators.compose([Validators.required, Validators.minLength(5)])),
        ],
        'login',
        '/dashboard');

    variableTest = {
        variable_test: 'variable test'
    };

    constructor(
        protected headerService: HeaderService,
        protected authService: AuthService,
        protected translate: TranslateService) {
    }

    ngOnInit() {
        // this.headerService.hide();
        this.authService.autoLogin();
    }

    async login(params): Promise<any> {
        const identifier = params.identifier;
        const password = params.password;

        const serverResponse = await this.authService.login(identifier, password).toPromise();

        console.log(serverResponse);
        // Save data here VERY hacky way
        if (!serverResponse.errors) {
            this.authService.saveUserData(serverResponse.data.login);
        }

        return serverResponse;
    }

    changeLang(lang: string) {
        this.translate.use(lang);
    }
}
