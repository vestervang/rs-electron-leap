import {Component, OnInit} from '@angular/core';
import {HeaderService} from '../services/header.service';
import {AuthService} from '../services/auth.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    loggedIn: boolean;

    constructor(
        public headerService: HeaderService,
        protected authService: AuthService
    ) {}

    ngOnInit() {
        this.authService.isAuthenticated
            .subscribe(isAuthenticated => {
                this.loggedIn = isAuthenticated;
            });
    }

    logout() {
        this.authService.logout();
    }

}
