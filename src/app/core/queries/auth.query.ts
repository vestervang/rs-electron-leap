import gql from 'graphql-tag';

export const CircusQuery = gql`
    {
        circus{
            location
            image
            last_wednesday
            next_wednesday
        }
    }
`;

