import gql from 'graphql-tag';

export const EmployeeList = gql`
    query {
    employees(first: 4){
        pageInfo{
            hasNextPage
            hasPreviousPage
            startCursor
            endCursor
            total
            count
            currentPage
            lastPage
        }
        edges{
            cursor
            node{
                id
                username
                email
                first_name
                last_name
            }
        }
    }
}
`;