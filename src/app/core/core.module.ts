import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {CoreRoutingModule} from './core-routing.module';
import {LoginComponent} from './login/login.component';
import {RouterModule} from '@angular/router';

import {NavigationComponent} from './navigation/navigation.component';
import { HeaderComponent } from './header/header.component';
import {SharedModule} from '../shared/shared.module';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CoreRoutingModule,
        SharedModule
    ],
    declarations: [
        LoginComponent,
        NavigationComponent,
        HeaderComponent,
        DashboardComponent
    ],
    exports: [
        RouterModule,
        HeaderComponent,
    ],
    providers: [
    ],
})
export class CoreModule {
}
