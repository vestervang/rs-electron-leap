import {Component, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
import {ElectronService} from './providers/electron.service';
import {AppConfig} from '../environments/environment';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    defaultTheme = 'default';
    activeTheme: string;
    cssFile: string;


    constructor(
        public electronService: ElectronService,
        public sanitizer: DomSanitizer,
        private translate: TranslateService
    ) {
        translate.setDefaultLang('da');
        // console.log('AppConfig', AppConfig);

        if (electronService.isElectron()) {
            // console.log('Mode electron');
            // console.log('Electron ipcRenderer', electronService.ipcRenderer);
            // console.log('NodeJS childProcess', electronService.childProcess);
        } else {
            // console.log('Mode web');
        }
    }

    ngOnInit() {
        // @TODO Need to implement both electron and web swithing!
        // const savedTheme = localStorage.getItem('theme');
        this.cssFile = './assets/css/themes/default/default.css';
    }
}
